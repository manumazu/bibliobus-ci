# Bibliobus CI

Containerize and build the Bibliobus project into your Linux architecture.

Edit a .env file for setting app config for following environment variables

```
#must set a strong key chain to encode / decode jwt
SECRET_KEY=[SECRET]
#key used for recaptcha v3, must be defined here : https://www.google.com/recaptcha/admin/
RECAPTCHA_SECRET=[SECRET]
#used for isbn search, key must be defined here : https://console.cloud.google.com/apis/credentials
GOOGLE_BOOK_API_KEY=[GOOLE_API]
#define email of account who is allowed to set up bibus modules 
SHELF_ADMIN_EMAIL = ''
#database config
DB_USER=[DB_USER] (default "bibliobus")
DB_PASSWORD=[DB_PASSWORD] (default "bibliobus")
DB_NAME=[DB_NAME] (default "bibliobus")
DB_ROOT_PASSWORD=[DB_ROOT_PASSWORD] (default "root")
DB_HOST=[DB_HOST] (default "db")
```

If you want to use default values without .env file, juste comment these variables in docker-compose.yml file


3 configurations are possible :

## Full embed

If the .env file is set, just do:

 `docker-compose up -d`
 
## Host based Source code 

Code source is shared with the container

- Get application sources inside a "app" directory

  `git clone git@github.com:manumazu/bilioapp.git app`
  
- Get latest image from Docker Hub or build image via compose and load services:

  `docker-compose -f docker-compose-host_based_sources.yml up -d`
  
 ## Host based database
 
 In order to use the host database server :
 
 - Export the db host ip address into the 'DOCKER_HOST_IP' environment variable:

 `export DOCKER_HOST_IP=$(hostname -I | awk '{print $1}')`
 
 - Launch containers (app and proxies only) :
 
  `docker-compose -f docker-compose-host_based_bdd.yml up -d`

NB : 
- for SSL you can set your own certificats (default app has auto-generated certificats) and set path into the `conf-proxy/nginx.conf` config file
- for MariaDB connection, set your own login / password into .env file or `biblioapp/config-init.py` before building image or into `biblioapp/config.py` after build if you need to change it

## Test Biblioapp using pytest

In order to test your application before deployment, it is possible to use pytest module as entrypoint :

`docker run -it --name bibliobus --entrypoint python manumazu/bibliobus:latest -m pytest tests/`

This command will run some unit and functionnal tests defined in app/tests directory 
