FROM python:slim

#comment if you have already a biblioapp instance in the same directory
RUN apt update
RUN apt install git -y
RUN git clone https://github.com/manumazu/biblioapp.git app
RUN cp app/config-init.py app/config.py

#uncomment if you have already a biblioapp instance in the same directory
#COPY app/config-init.py /app/config.py
#COPY app/requirements.txt /app
#COPY app/boot.sh /app/

WORKDIR /app
RUN ls -l .
RUN python -m pip install --upgrade pip
RUN python -m venv venv
RUN pip install -r requirements.txt
RUN pip install gunicorn

ENV FLASK_APP biblioapp

#run some unit tests to validate app structure
RUN python -m pytest tests/unit

RUN chmod +x boot.sh

EXPOSE 5000
ENTRYPOINT ["/app/boot.sh"]
